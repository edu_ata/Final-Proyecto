package com.example.proyecto.services;

import com.example.proyecto.entities.Comment;
import com.example.proyecto.entities.Restaurant;

import java.util.List;

public interface CommentService {

        Iterable<Comment> listAllComments();

        void saveComment(Comment comment);

        Comment getComment(Integer id);

        void deleteComment(Integer id);

        Integer getCommentsRestaurant(Integer id_restaurant);
}
