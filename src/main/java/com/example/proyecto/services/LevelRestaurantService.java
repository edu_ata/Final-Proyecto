package com.example.proyecto.services;


import com.example.proyecto.entities.LevelRestaurant;


public interface LevelRestaurantService {

    Iterable<LevelRestaurant> listAllLevelRestaurants();

    void saveLevelRestaurant(LevelRestaurant levelRestaurant);

    LevelRestaurant getLevelRestaurant(Integer id);

    void deleteLevelRestaurant(Integer id);
}
