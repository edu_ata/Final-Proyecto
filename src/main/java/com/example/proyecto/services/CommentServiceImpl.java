package com.example.proyecto.services;

import com.example.proyecto.entities.Comment;
import com.example.proyecto.entities.Restaurant;
import com.example.proyecto.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class CommentServiceImpl implements CommentService{

    private CommentRepository commentRepository;
    @Autowired
    @Qualifier(value = "commentRepository")
    public void setCommentRepository(CommentRepository commentRepository) {
                this.commentRepository = commentRepository;
    }


    @Override
    public Iterable<Comment> listAllComments() {
                return commentRepository.findAll();
    }

    @Override
    public void saveComment(Comment comment) {
                commentRepository.save(comment);
    }

    @Override
    public Comment getComment(Integer id) {
                return commentRepository.findById(id).get();
    }

    @Override
    public void deleteComment(Integer id) {
                commentRepository.deleteById(id);;
    }

    @Override
    public Integer getCommentsRestaurant(Integer id_restaurant) {
        Iterable<Comment> comments=listAllComments();
        Integer sumaEstrellas = 0;
        Integer cantComentarios = 0;
        for (Comment comment : comments) {
            if (comment.getRestaurant().getId() == id_restaurant) {
                sumaEstrellas += comment.getRating();
                cantComentarios++;
            }
        }

        return sumaEstrellas/cantComentarios;
    }






}
