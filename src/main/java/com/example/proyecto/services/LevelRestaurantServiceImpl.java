package com.example.proyecto.services;

import com.example.proyecto.entities.LevelRestaurant;
import com.example.proyecto.repository.LevelRestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class LevelRestaurantServiceImpl implements LevelRestaurantService {

    private LevelRestaurantRepository levelRestaurantRepository;

    @Autowired
    @Qualifier(value="levelRestaurantRepository")
    public void setLevelRestaurantRepository(LevelRestaurantRepository levelRestaurantRepository) {
        this.levelRestaurantRepository = levelRestaurantRepository;
    }


    @Override
    public Iterable<LevelRestaurant> listAllLevelRestaurants() {
        return levelRestaurantRepository.findAll();
    }

    @Override
    public void saveLevelRestaurant(LevelRestaurant levelRestaurant) {
        levelRestaurantRepository.save(levelRestaurant);
    }

    @Override
    public LevelRestaurant getLevelRestaurant(Integer id) {
        return levelRestaurantRepository.findById(id).get();
    }

    @Override
    public void deleteLevelRestaurant(Integer id) {
        levelRestaurantRepository.deleteById(id);
    }


}
