package com.example.proyecto.repository;

import com.example.proyecto.entities.Category;
import com.example.proyecto.entities.City;
import com.example.proyecto.entities.Restaurant;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Locale;

@Transactional
public interface RestaurantRepository extends CrudRepository<Restaurant,Integer>{

    @Query("select r from Restaurant r where r.name like %:name%")
    Iterable<Restaurant> getRestaurantsLikeName(@Param("name") String name);

    List<Restaurant> findByCategory(Category category);

    List<Restaurant> findByCity(City city);
}
