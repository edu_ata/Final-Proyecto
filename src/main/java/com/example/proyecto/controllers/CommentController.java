package com.example.proyecto.controllers;
import com.example.proyecto.entities.*;
import com.example.proyecto.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Controller
public class CommentController {
        private CommentService commentService;
        private UserService userService;

        @Autowired
        public void setCommentService(CommentService commentService) {
                this.commentService = commentService;
        }
        @Autowired
        public void setUserService(UserService userService){this.userService=userService;}
        @RequestMapping(value = "/comment", method = RequestMethod.POST)
        String save(Comment comment) {
                Authentication auth=SecurityContextHolder.getContext().getAuthentication();
                User user=userService.findByUsername(auth.getName());
                comment.setUser(user);
                if(comment.getUser()==null)
                {
                        System.out.println("Error");
                }
                else
                {
                        System.out.println("User id :"+ comment.getUser().getId());
                }
                commentService.saveComment(comment);
                return "redirect:/restaurant/"+comment.getRestaurant().getId();
            }


}
