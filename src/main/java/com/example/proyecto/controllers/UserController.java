package com.example.proyecto.controllers;

import com.example.proyecto.entities.*;
import com.example.proyecto.services.*;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.Tuple;
import javax.validation.constraints.Null;
import java.util.*;

@Controller
public class UserController {


    @Autowired
    private UserService userService;
    @Autowired
    private RestaurantService restaurantService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CityService cityService;
    @Autowired
    private CommentService commentService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    public void setRestaurantService(RestaurantService restaurantService) { this.restaurantService = restaurantService; }
    public void setCategoryService(CategoryService categoryService) { this.categoryService = categoryService; }
    public void setCityService(CityService cityService) { this.cityService = cityService; }
    public void setComentService(CommentService commentService) { this.commentService = commentService; }


    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registrationInit(Model model) {
        model.addAttribute("user", new User());
        Iterable<City> cities=cityService.listAllCitys();
        model.addAttribute("cities",cities);
        return "register";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("user") User user, BindingResult bindingResult, Model model) {
        ///userValidator.validate(userForm, bindingResult);
        Iterable<City> cities=cityService.listAllCitys();
        model.addAttribute("cities",cities);
        if (bindingResult.hasErrors()) {
            return "register";
        }
        userService.save(user);
        return "redirect:/";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String welcome(@RequestParam(value = "name", required = false, defaultValue="") String name,
                          Model model, String logout) {


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.User u = (org.springframework.security.core.userdetails.User)auth.getPrincipal();
        com.example.proyecto.entities.User user = userService.findByUsername(u.getUsername());
        model.addAttribute("user",user);


        Iterable<Restaurant> restaurantListSearch = null;
        Iterable<Category> categories= categoryService.listAllCategorys();
        model.addAttribute("categories",categories);
        Iterable<City> cities=cityService.listAllCitys();
        model.addAttribute("cities",cities);
        List<Restaurant> myCity=restaurantService.findByCity(user.getCity());
        model.addAttribute("myCity",myCity);



        /*if (name!=""){
            restaurantListSearch =restaurantService.getRestaurantsLikeName(name);
        }
    /*
        List<Comment> comments = (ArrayList)commentService.listAllComments();

        List<Pair<Integer, Restaurant>> search = new ArrayList<>();

        for (Restaurant restaurant : restaurantListSearch) {
            Integer sumaEstrellas = 0;
            Integer cantComentarios = 0;

            for (Comment comment : comments) {
                if (comment.getRestaurant().getId() == restaurant.getId()) {
                    sumaEstrellas += comment.getRating();
                    cantComentarios++;
                }
            }

            Pair<Integer, Restaurant> nuevoDato;

            if (cantComentarios == 0) {
                nuevoDato = new Pair<>(0, restaurant);
            } else {
                nuevoDato = new Pair<>(new Integer(sumaEstrellas / cantComentarios), restaurant);
            }

            search.add(nuevoDato);
        }

        List<Pair<String, Restaurant>> searchFinal = new ArrayList<>();

        for (Pair<Integer , Restaurant> pair : search) {
            String stars = "";
            for (int i = 0; i < pair.getKey(); i++) {
                stars += "★";
            }

            Pair<String, Restaurant> nuevoDato = new Pair<>(stars, pair.getValue());

            // ORDENAR
            /*if (!searchFinal.isEmpty()) {
                if (pair.getKey() > new Integer(searchFinal.get(0).getKey())) {
                    searchFinal.add(0, nuevoDato);
                } else {
                    searchFinal.add(nuevoDato);
                }
            }*/

        //searchFinal.add(nuevoDato);
        //}


        model.addAttribute("search", restaurantListSearch);
        return "index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        return "login";
    }


    @RequestMapping(value = "/buscarByName")
    String buscarByName(@RequestParam(value = "name", required = false, defaultValue="") String name, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.User u = (org.springframework.security.core.userdetails.User)auth.getPrincipal();
        com.example.proyecto.entities.User user = userService.findByUsername(u.getUsername());
        model.addAttribute("user",user);
        List<Restaurant> myCity=null;
        model.addAttribute("myCity",myCity);
        Iterable<Category> categories= categoryService.listAllCategorys();
        model.addAttribute("categories",categories);
        Iterable<City> cities=cityService.listAllCitys();
        model.addAttribute("cities",cities);

        Iterable<Restaurant> restaurantListSearch = null;

        if (name!=""){
            restaurantListSearch =restaurantService.getRestaurantsLikeName(name);
        }

        List<Comment> comments = (ArrayList)commentService.listAllComments();

        List<Pair<Integer, Restaurant>> search = new ArrayList<>();

        for (Restaurant restaurant : restaurantListSearch) {
            Integer sumaEstrellas = 0;
            Integer cantComentarios = 0;

            for (Comment comment : comments) {
                if (comment.getRestaurant().getId() == restaurant.getId()) {
                    sumaEstrellas += comment.getRating();
                    cantComentarios++;
                }
            }

            Pair<Integer, Restaurant> nuevoDato;

            if (cantComentarios == 0) {
                nuevoDato = new Pair<>(0, restaurant);
            } else {
                nuevoDato = new Pair<>(new Integer(sumaEstrellas / cantComentarios), restaurant);
            }

            search.add(nuevoDato);
        }

        List<Pair<String, Restaurant>> searchFinal1 = new ArrayList<>();
        List<Pair<String, Restaurant>> searchFinal2 = new ArrayList<>();
        List<Pair<String, Restaurant>> searchFinal3 = new ArrayList<>();
        List<Pair<String, Restaurant>> searchFinal4 = new ArrayList<>();
        List<Pair<String, Restaurant>> searchFinal5 = new ArrayList<>();

        for (Pair<Integer , Restaurant> pair : search) {
            String stars = "";
            for (int i = 0; i < pair.getKey(); i++) {
                stars += "★";
            }

            Pair<String, Restaurant> nuevoDato = new Pair<>(stars, pair.getValue());

            // ORDENAR
            /*if (!searchFinal.isEmpty()) {
                if (pair.getKey() > new Integer(searchFinal.get(0).getKey())) {
                    searchFinal.add(0, nuevoDato);
                } else {
                    searchFinal.add(nuevoDato);
                }
            }*/

            switch (pair.getKey()) {
                case 1:
                    searchFinal1.add(nuevoDato);
                    break;
                case 2:
                    searchFinal2.add(nuevoDato);
                    break;
                case 3:
                    searchFinal3.add(nuevoDato);
                    break;
                case 4:
                    searchFinal4.add(nuevoDato);
                    break;
                case 5:
                    searchFinal5.add(nuevoDato);
                    break;
                default:
                    // NADA
                    break;

            }
        }

        searchFinal1.isEmpty();

        model.addAttribute("search1", searchFinal1);
        model.addAttribute("search2", searchFinal2);
        model.addAttribute("search3", searchFinal3);
        model.addAttribute("search4", searchFinal4);
        model.addAttribute("search5", searchFinal5);

        return "index";
    }

    @RequestMapping(value = "/buscarCategory/{category_id}")
    String buscarByCategory(@PathVariable Integer category_id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.User u = (org.springframework.security.core.userdetails.User)auth.getPrincipal();
        com.example.proyecto.entities.User user = userService.findByUsername(u.getUsername());
        model.addAttribute("user",user);
        List<Restaurant> myCity=null;
        model.addAttribute("myCity",myCity);
        Iterable<Category> categories= categoryService.listAllCategorys();
        model.addAttribute("categories",categories);
        Iterable<City> cities=cityService.listAllCitys();
        model.addAttribute("cities",cities);

        Category category=categoryService.getCategory(category_id);
        List<Restaurant> searchCategory=restaurantService.findByCategory(category);

        List<Comment> comments = (ArrayList)commentService.listAllComments();

        List<Pair<Integer, Restaurant>> search = new ArrayList<>();

        for (Restaurant restaurant : searchCategory) {
            Integer sumaEstrellas = 0;
            Integer cantComentarios = 0;

            for (Comment comment : comments) {
                if (comment.getRestaurant().getId() == restaurant.getId()) {
                    sumaEstrellas += comment.getRating();
                    cantComentarios++;
                }
            }

            Pair<Integer, Restaurant> nuevoDato;

            if (cantComentarios == 0) {
                nuevoDato = new Pair<>(0, restaurant);
            } else {
                nuevoDato = new Pair<>(new Integer(sumaEstrellas / cantComentarios), restaurant);
            }

            search.add(nuevoDato);
        }

        List<Pair<String, Restaurant>> searchFinal = new ArrayList<>();

        for (Pair<Integer , Restaurant> pair : search) {
            String stars = "";
            for (int i = 0; i < pair.getKey(); i++) {
                stars += "★";
            }

            Pair<String, Restaurant> nuevoDato = new Pair<>(stars, pair.getValue());

            searchFinal.add(nuevoDato);
        }

        model.addAttribute("search",searchFinal);
        return "index";
    }

    @RequestMapping(value = "/buscarCity/{city_id}")
    String buscarByCity(@PathVariable Integer city_id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.User u = (org.springframework.security.core.userdetails.User)auth.getPrincipal();
        com.example.proyecto.entities.User user = userService.findByUsername(u.getUsername());
        model.addAttribute("user",user);
        List<Restaurant> myCity=null;
        model.addAttribute("myCity",myCity);
        Iterable<Category> categories= categoryService.listAllCategorys();
        model.addAttribute("categories",categories);
        Iterable<City> cities=cityService.listAllCitys();
        model.addAttribute("cities",cities);

        City city=cityService.getCity(city_id);
        List<Restaurant> searchCity=restaurantService.findByCity(city);
        List<Comment> comments = (ArrayList)commentService.listAllComments();

        List<Pair<Integer, Restaurant>> search = new ArrayList<>();

        for (Restaurant restaurant : searchCity) {
            Integer sumaEstrellas = 0;
            Integer cantComentarios = 0;

            for (Comment comment : comments) {
                if (comment.getRestaurant().getId() == restaurant.getId()) {
                    sumaEstrellas += comment.getRating();
                    cantComentarios++;
                }
            }

            Pair<Integer, Restaurant> nuevoDato;

            if (cantComentarios == 0) {
                nuevoDato = new Pair<>(0, restaurant);
            } else {
                nuevoDato = new Pair<>(new Integer(sumaEstrellas / cantComentarios), restaurant);
            }

            search.add(nuevoDato);
        }

        List<Pair<String, Restaurant>> searchFinal = new ArrayList<>();

        for (Pair<Integer , Restaurant> pair : search) {
            String stars = "";
            for (int i = 0; i < pair.getKey(); i++) {
                stars += "★";
            }

            Pair<String, Restaurant> nuevoDato = new Pair<>(stars, pair.getValue());

            searchFinal.add(nuevoDato);
        }

        model.addAttribute("search",searchFinal);
        return "index";
    }
    @RequestMapping("/editUser/{id}")
    String editUsuario(@PathVariable Integer id,Model model) {

        User user=userService.getUser(id);
        model.addAttribute("user",user);
        Iterable<City> cities=cityService.listAllCitys();
        model.addAttribute("cities",cities);
        return "editUser";
    }


}
