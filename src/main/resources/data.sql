delete from category;
delete from restaurant;
delete from city;
delete from user;
delete from level_restaurant;

INSERT INTO user(id,email, name,password,password_confirm,role,username,city_id) VALUES (1, 'ojnc@hotmail.com','oliver','$2a$10$lTk0qHB/UAQkUTR5M0tE3OlnQUgimWwdUt86/aTlwUT7bXPV0o10O','pantalla','CLIENT','ojnc',1000);
INSERT INTO user(id,email, name,password,password_confirm,role,username,city_id) VALUES (22, 'administrador@gmail.com','admin','$2a$10$.jZV1xBZ3jaJMSlBunlqLuzNIabL6lFb4d7Nz7CeCC1z7SQFfh9qW','admin','ADMIN','admin',1001);
INSERT INTO user(id,email, name,password,password_confirm,role,username,city_id) VALUES (2, 'edwin@hotmail.com','edwin','$2a$10$JPLi0GdQjIfn6Qtyi8Iwv.MI2JOk6RDco3zTtdm1mBIrskiAmHBqa','ventana','CLIENT','edwin',1002);

INSERT INTO category(id, name) VALUES (1000, 'Desayuno');
INSERT INTO category(id, name) VALUES (1001, 'Almuerzo');
INSERT INTO category(id, name) VALUES (1002, 'Cena Familiar');
INSERT INTO category(id, name) VALUES (1003, 'Chino');
INSERT INTO city(id, name) VALUES (1000, 'Cochabamba');
INSERT INTO city(id, name) VALUES (1001, 'La Paz');
INSERT INTO city(id, name) VALUES (1002, 'Sucre');

INSERT INTO level_restaurant(id, name,rating) VALUES (1, 'Impopular',1);
INSERT INTO level_restaurant(id, name,rating) VALUES (2, 'Poco Popular',2);
INSERT INTO level_restaurant(id, name,rating) VALUES (3, 'Intermedio',3);
INSERT INTO level_restaurant(id, name,rating) VALUES (4, 'Popular',4);
INSERT INTO level_restaurant(id, name,rating) VALUES (5, 'MUy Popular',5);

INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion,latitud,longitud) VALUES (1001, 'HyperMaxi',1000,1000,'buffet','foto1','cbba',-17.3784403,-66.1484966);
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion,latitud,longitud) VALUES (1002, 'IceNorte',1001,1001,'almuerzos','foto2','cbba',-17.378003,-66.1480066);
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion,latitud,longitud,level_restaurant_id) VALUES (1003, 'Planchitas',1002,1002,'platos variados','foto3','cbba',-17.3084403,-66.1404966,3);
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion,latitud,longitud,level_restaurant_id) VALUES (1004, 'Pizzeria Malcriada ',1000,1002,'platos variados','foto3','cbba',-17.3784400,-66.1484906,4);
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion,latitud,longitud,level_restaurant_id) VALUES (1005, 'Los 4 Toros',1001,1000,'platos variados','foto3','cbba',-17.3784403,-66.1484966,5);
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion,latitud,longitud,level_restaurant_id) VALUES (1006, 'Jakaranda',1003,1001,'platos variados','foto3','cbba',-17.3704403,-66.1484966,4);
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion,latitud,longitud) VALUES (1007, 'El palacio del Sillapncho',1000,1002,'platos variados','foto3','cbba',-17.3784403,-66.1484066);
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion,latitud,longitud) VALUES (1008, 'Panchita',1001,1000,'platos variados','foto3','cbba',-17.3780003,-66.1484906);
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion,latitud,longitud) VALUES (1009, 'Pollos KFC',1002,1001,'platos variados','foto3','cbba',-17.3004403,-66.1484006);
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion,latitud,longitud) VALUES (1010, 'Kingdong',1003,1002,'platos variados','foto3','cbba',-17.3784003,-66.1484966);
